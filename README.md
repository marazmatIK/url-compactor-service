# URL Compactor Web App

## What is this?
Spring powered Web App for compacting (or should we say shortening) URLs provided by user via submit form.

## Components

## Build Guide

## Run & Configuration Guide

## TODO:
1. Documentation
2. Maven profiles: dev, prod, qa, demo/debug etc.
3. Different DataSource confogurations and application.properties
4. Prodive -Dproperties support for dynamic flexible configuration
5. Resolve duplicated origin URLs problem (give already shortened link, why not?)
6. More test, including integration ones
7. NEW Feature (?): Custom User Short links. Let the user generate short link using his own "pattern"
8. CI Pipeline (?): Configure remote Node to check each pushed tag: detect push, build and run tests, generate reports, provide artifacts, notify all involved parties, provide metrics, history etc. Jenkins node?